package com.vinaysshenoy.spekex

import android.content.res.Resources

fun dpToPx(dp: Float) = dp * Resources.getSystem().displayMetrics.density

fun pxToDp(px: Float) = px / Resources.getSystem().displayMetrics.density