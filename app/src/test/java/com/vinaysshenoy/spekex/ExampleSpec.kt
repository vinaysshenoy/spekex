package com.vinaysshenoy.spekex

import org.amshove.kluent.shouldEqualTo
import org.amshove.kluent.shouldNotEqual
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

object ExampleSpec: Spek({

  describe("A value") {
    val number = 2

    on("adding it to another value") {
      val sum = number + 5

      it("should set the right values") {
        sum shouldEqualTo 7
      }

      it("should not set the wrong value") {
        sum shouldNotEqual 2
      }
    }
  }
})
