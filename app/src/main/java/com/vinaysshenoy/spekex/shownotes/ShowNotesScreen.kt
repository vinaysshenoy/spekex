package com.vinaysshenoy.spekex.shownotes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.vinaysshenoy.spekex.Controller
import com.vinaysshenoy.spekex.MviView
import com.vinaysshenoy.spekex.R
import com.vinaysshenoy.spekex.TheApplication
import com.vinaysshenoy.spekex.ViewControllerBinding
import com.vinaysshenoy.spekex.shownotes.LoadNoteStatus.LOADING
import com.vinaysshenoy.spekex.shownotes.LoadNoteStatus.REFRESHING
import kotlinx.android.synthetic.main.screen_shownotes.btn_addNote
import kotlinx.android.synthetic.main.screen_shownotes.container_notesList
import kotlinx.android.synthetic.main.screen_shownotes.list_notes
import timber.log.Timber

private val refreshingStates = setOf(LOADING, REFRESHING)

class ShowNotesScreen : Fragment(), MviView<ShowNotesViewState, ShowNotesSideEffect> {

  private lateinit var controller: Controller<ShowNotesEvent, ShowNotesViewState, ShowNotesSideEffect>

  private lateinit var notesAdapter: NotesListAdapter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val serviceLocator = TheApplication.serviceLocator
    controller = ShowNotesController(serviceLocator.notesRepository)
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    viewLifecycleOwner.lifecycle.addObserver(ViewControllerBinding(controller, this))
    return inflater.inflate(R.layout.screen_shownotes, container, false)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    setupNotesAdapter()
    btn_addNote.setOnClickListener { controller.push(ShowNotesEvent.AddNote) }
  }

  private fun setupNotesAdapter() {
    notesAdapter = NotesListAdapter { Timber.d("Note Clicked: $it") }

    list_notes.setHasFixedSize(true)
    list_notes.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    list_notes.adapter = notesAdapter
  }

  override fun render(state: ShowNotesViewState) {
    Timber.tag("BUG").d("Render: ${state.loadNoteStatus}")
    container_notesList.isRefreshing = state.loadNoteStatus in refreshingStates
    notesAdapter.submitList(state.notes)
  }

  override fun sideEffect(sideEffect: ShowNotesSideEffect) {
    Timber.d("Side Effect: $sideEffect")
  }
}
