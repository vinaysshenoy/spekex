package com.vinaysshenoy.spekex

class FakeReceiver<T: Any> : Receiver<T> {

  var received = emptyList<T>()
    private set

  val latest: T
    get() = received.last()

  val lastButOne: T
    get() = received[received.lastIndex - 1]

  val hasChanges: Boolean
    get() = received.isNotEmpty()

  override fun receive(item: T) {
    received += item
  }

  fun reset() {
    received = emptyList()
  }
}