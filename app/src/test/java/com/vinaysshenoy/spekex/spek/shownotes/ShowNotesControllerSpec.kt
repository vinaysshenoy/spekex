package com.vinaysshenoy.spekex.spek.shownotes

import com.vinaysshenoy.spekex.FakeReceiver
import com.vinaysshenoy.spekex.data.Note
import com.vinaysshenoy.spekex.data.Timestamp
import com.vinaysshenoy.spekex.mocks.MockNoteRepository
import com.vinaysshenoy.spekex.shownotes.LoadNoteError
import com.vinaysshenoy.spekex.shownotes.LoadNoteStatus
import com.vinaysshenoy.spekex.shownotes.ShowNotesController
import com.vinaysshenoy.spekex.shownotes.ShowNotesEvent
import com.vinaysshenoy.spekex.shownotes.ShowNotesSideEffect
import com.vinaysshenoy.spekex.shownotes.ShowNotesViewState
import kotlinx.coroutines.experimental.Unconfined
import kotlinx.coroutines.experimental.runBlocking
import org.amshove.kluent.shouldBeFalse
import org.amshove.kluent.shouldBeTrue
import org.amshove.kluent.shouldEqual
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.jetbrains.spek.api.lifecycle.CachingMode
import java.util.Date
import java.util.UUID

object ShowNotesControllerSpec : Spek({

  describe("setup") {

    val noteTemplate = Note(
        id = UUID.randomUUID(),
        title = "",
        content = "",
        timestamp = Timestamp()
    )

    val stateReceiver by memoized { FakeReceiver<ShowNotesViewState>() }
    val sideEffectReceiver by memoized { FakeReceiver<ShowNotesSideEffect>() }

    describe("a NoteRepository that works") {
      val date = Date()
      val notes = listOf(
          noteTemplate.copy(UUID.randomUUID(), timestamp = noteTemplate.timestamp.withUpdatedOn(Date(date.time))),
          noteTemplate.copy(UUID.randomUUID(), timestamp = noteTemplate.timestamp.withUpdatedOn(Date(date.time + 1))),
          noteTemplate.copy(UUID.randomUUID(), timestamp = noteTemplate.timestamp.withUpdatedOn(Date(date.time + 2)))
      )

      val repository by memoized(CachingMode.SCOPE) {
        object : MockNoteRepository() {
          override fun allNotes(newestFirst: Boolean) =
              if (newestFirst) listOf(notes[2], notes[1], notes[0]) else listOf(notes[0], notes[1], notes[2])
        }
      }

      val controller by memoized {
        ShowNotesController(
            noteRepository = repository,
            background = Unconfined,
            io = Unconfined,
            ui = Unconfined
        )
      }

      group("initializing the controller") {

        beforeEachTest {
          controller.registerStateReceiver(stateReceiver, true)
          controller.registerSideEffectReceiver(sideEffectReceiver)
        }

        on("initializing") {

          it("should have changes") {
            stateReceiver.hasChanges.shouldBeTrue()
          }

          it("should emit the initial state") {
            stateReceiver.received.first() shouldEqual ShowNotesViewState(
                loadNoteStatus = LoadNoteStatus.LOADING,
                notes = emptyList(),
                loadNoteError = LoadNoteError.NO_ERROR
            )
          }

          it("must not emit any sideEffects actions") {
            sideEffectReceiver.hasChanges.shouldBeFalse()
          }

          it("must load notes in the order of latest updated") {
            val expected = listOf(notes[2], notes[1], notes[0])

            stateReceiver.latest shouldEqual ShowNotesViewState(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = expected, loadNoteError = LoadNoteError.NO_ERROR)
          }
        }
      }

      group("refreshing notes with previous error") {

        beforeEachTest {
          controller.registerStateReceiver(stateReceiver, false)
          controller.registerSideEffectReceiver(sideEffectReceiver)
        }

        val testCases = mapOf(
            LoadNoteStatus.LOAD_FAIL to LoadNoteError.NO_ERROR,
            LoadNoteStatus.REFRESH_FAIL to LoadNoteError.NO_ERROR
        )

        testCases.forEach { loadNoteStatus, expectedLoadNoteError ->
          on("previous error is $loadNoteStatus") {
            val oldViewState = ShowNotesViewState(
                loadNoteStatus = loadNoteStatus,
                notes = emptyList(),
                loadNoteError = LoadNoteError.DATABASE_ERROR
            )

            runBlocking { controller.reducer.offer { oldViewState } }

            controller.push(ShowNotesEvent.RefreshNotes)

            it("should reset the load note error to $expectedLoadNoteError when emitting the refreshing state") {
              stateReceiver.lastButOne shouldEqual ShowNotesViewState(
                  loadNoteStatus = LoadNoteStatus.REFRESHING,
                  notes = emptyList(),
                  loadNoteError = expectedLoadNoteError
              )
            }

            it("should emit the refreshed view state with the notes ordered by latest updated") {
              val expected = listOf(notes[2], notes[1], notes[0])
              stateReceiver.latest shouldEqual ShowNotesViewState(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = expected, loadNoteError = LoadNoteError.NO_ERROR)
            }
          }
        }
      }

      group("refreshing notes") {

        beforeEachTest {
          controller.registerStateReceiver(stateReceiver, false)
          controller.registerSideEffectReceiver(sideEffectReceiver)
        }

        on("refreshing notes without previous error") {
          val oldViewState = ShowNotesViewState(
              loadNoteStatus = LoadNoteStatus.SUCCESS,
              notes = listOf(noteTemplate.copy(UUID.randomUUID())),
              loadNoteError = LoadNoteError.NO_ERROR
          )

          runBlocking { controller.reducer.offer { oldViewState } }

          controller.push(ShowNotesEvent.RefreshNotes)

          it("should emit the refreshing view state") {
            stateReceiver.lastButOne shouldEqual ShowNotesViewState(loadNoteStatus = LoadNoteStatus.REFRESHING, notes = oldViewState.notes, loadNoteError = LoadNoteError.NO_ERROR)
          }

          it("should emit the refreshed state with the notes ordered by latest updated") {
            val expected = listOf(notes[2], notes[1], notes[0])
            stateReceiver.latest shouldEqual ShowNotesViewState(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = expected, loadNoteError = LoadNoteError.NO_ERROR)
          }
        }
      }

      group("side effects") {

        beforeEachTest {
          controller.registerStateReceiver(stateReceiver, false)
          controller.registerSideEffectReceiver(sideEffectReceiver)
        }

        on("pushing the add note action") {
          controller.push(ShowNotesEvent.AddNote)

          it("should emit the open note create sideEffects event") {
            sideEffectReceiver.latest shouldEqual ShowNotesSideEffect.OpenCreateNoteScreen
          }
        }

        on("pushing the open note action") {
          val noteToOpen = notes[0]
          controller.push(ShowNotesEvent.OpenNote(noteToOpen))

          it("should emit the show note screen with the note id") {
            sideEffectReceiver.latest shouldEqual ShowNotesSideEffect.ShowNoteScreen(noteToOpen.id)
          }
        }
      }
    }

    describe("a repository that fails") {
      val repository by memoized(CachingMode.SCOPE) {
        object : MockNoteRepository() {
          override fun allNotes(newestFirst: Boolean): List<Note> {
            throw RuntimeException("Test Exception")
          }
        }
      }

      val controller by memoized {
        ShowNotesController(
            noteRepository = repository,
            background = Unconfined,
            io = Unconfined,
            ui = Unconfined
        )
      }

      group("initializing the controller") {
        beforeEachTest {
          controller.registerStateReceiver(stateReceiver, true)
          controller.registerSideEffectReceiver(sideEffectReceiver)
        }

        on("initializing the controller") {
          it("must emit the failed state") {
            stateReceiver.latest shouldEqual ShowNotesViewState(
                loadNoteStatus = LoadNoteStatus.LOAD_FAIL,
                notes = emptyList(),
                loadNoteError = LoadNoteError.DATABASE_ERROR
            )
          }
        }
      }

      group("refreshing notes") {
        beforeEachTest {
          controller.registerStateReceiver(stateReceiver, false)
          controller.registerSideEffectReceiver(sideEffectReceiver)
        }

        on("refreshing notes with previous notes present") {
          val oldViewState = ShowNotesViewState(
              loadNoteStatus = LoadNoteStatus.SUCCESS,
              notes = listOf(noteTemplate.copy(UUID.randomUUID())),
              loadNoteError = LoadNoteError.NO_ERROR
          )

          runBlocking { controller.reducer.offer { oldViewState } }
          controller.push(ShowNotesEvent.RefreshNotes)

          it("should emit the failed state with the previous notes") {
            stateReceiver.latest shouldEqual ShowNotesViewState(
                loadNoteStatus = LoadNoteStatus.REFRESH_FAIL,
                notes = oldViewState.notes,
                loadNoteError = LoadNoteError.DATABASE_ERROR
            )
          }
        }
      }
    }
  }
})
