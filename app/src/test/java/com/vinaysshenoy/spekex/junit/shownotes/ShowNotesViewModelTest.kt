package com.vinaysshenoy.spekex.junit.shownotes

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.vinaysshenoy.spekex.TestObserver
import com.vinaysshenoy.spekex.data.Note
import com.vinaysshenoy.spekex.data.NoteRepositoryImpl
import com.vinaysshenoy.spekex.data.Timestamp
import com.vinaysshenoy.spekex.shownotes.LoadNoteError
import com.vinaysshenoy.spekex.shownotes.LoadNoteStatus
import com.vinaysshenoy.spekex.shownotes.ShowNotesEvent
import com.vinaysshenoy.spekex.shownotes.ShowNotesSideEffect
import com.vinaysshenoy.spekex.shownotes.ShowNotesViewModel
import com.vinaysshenoy.spekex.shownotes.ShowNotesViewState
import com.vinaysshenoy.spekex.threading.AsyncExecutor
import org.amshove.kluent.shouldBe
import org.amshove.kluent.shouldBeFalse
import org.amshove.kluent.shouldBeNull
import org.amshove.kluent.shouldBeTrue
import org.amshove.kluent.shouldEqual
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.Date
import java.util.UUID

class ShowNotesViewModelTest {

  @JvmField
  @Rule
  val rule1 = InstantTaskExecutorRule()

  private val noteTemplate = Note(
      id = UUID.randomUUID(),
      title = "",
      content = "",
      timestamp = Timestamp()
  )

  private val async = AsyncExecutor(AsyncExecutor.SynchronousDelegate())
  private val stateObserver = TestObserver<ShowNotesViewState>()
  private val navigationObserver = TestObserver<ShowNotesSideEffect>()

  private lateinit var repository: NoteRepositoryImpl
  private lateinit var viewModel: ShowNotesViewModel

  @Before
  fun setup() {
    val date = Date()
    repository = NoteRepositoryImpl(
        notes = listOf(
            noteTemplate.copy(UUID.randomUUID(), timestamp = noteTemplate.timestamp.withUpdatedOn(Date(date.time))),
            noteTemplate.copy(UUID.randomUUID(), timestamp = noteTemplate.timestamp.withUpdatedOn(Date(date.time + 1))),
            noteTemplate.copy(UUID.randomUUID(), timestamp = noteTemplate.timestamp.withUpdatedOn(Date(date.time + 2)))
        )
    )

    viewModel = ShowNotesViewModel(repository, async, stateObserver)
    viewModel.navigationEvents().observeForever(navigationObserver)
  }

  @After
  fun teardown() {
    viewModel.pull().removeObserver(stateObserver)
    viewModel.navigationEvents().removeObserver(navigationObserver)
    stateObserver.reset()
    navigationObserver.reset()
  }

  @Test
  fun `the view model must emit the base state on initialization`() {
    stateObserver.hasChanges.shouldBeTrue()
    stateObserver.changes.first() shouldEqual ShowNotesViewState(
        loadNoteStatus = LoadNoteStatus.LOADING,
        notes = emptyList(),
        loadNoteError = LoadNoteError.NO_ERROR
    )
  }

  @Test
  fun `the view model must not emit any navigation actions on initialization`() {
    viewModel.navigationActions.value.shouldBeNull()
    navigationObserver.hasChanges.shouldBeFalse()
  }

  @Test
  fun `the view model must load the notes in the order of latest updated on initialization`() {
    val expected = listOf(
        repository.notes[2],
        repository.notes[1],
        repository.notes[0]
    )

    stateObserver.hasChanges.shouldBeTrue()
    stateObserver.latest shouldEqual ShowNotesViewState(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = expected, loadNoteError = LoadNoteError.NO_ERROR)
  }

  @Test
  fun `on refresh notes, the view model must emit the refreshing state before the refreshed state`() {
    val oldViewState = ShowNotesViewState(
        loadNoteStatus = LoadNoteStatus.SUCCESS,
        notes = listOf(noteTemplate.copy(UUID.randomUUID())),
        loadNoteError = LoadNoteError.NO_ERROR
    )
    viewModel.stateHolder.state = oldViewState

    viewModel.push(ShowNotesEvent.RefreshNotes)
    stateObserver.lastButOne shouldEqual ShowNotesViewState(
        loadNoteStatus = LoadNoteStatus.REFRESHING,
        notes = oldViewState.notes,
        loadNoteError = LoadNoteError.NO_ERROR
    )
  }

  @Test
  fun `on refresh notes, the view model must reset any load note error that was pending when emitting the refreshing state`() {
    val oldViewState = ShowNotesViewState(
        loadNoteStatus = LoadNoteStatus.LOAD_FAIL,
        notes = emptyList(),
        loadNoteError = LoadNoteError.DATABASE_ERROR
    )
    viewModel.stateHolder.state = oldViewState

    viewModel.push(ShowNotesEvent.RefreshNotes)
    stateObserver.lastButOne shouldEqual ShowNotesViewState(
        loadNoteStatus = LoadNoteStatus.REFRESHING,
        notes = oldViewState.notes,
        loadNoteError = LoadNoteError.NO_ERROR
    )
  }

  @Test
  fun `on refresh notes, the view model must reset any refresh note error that was pending when emitting the refreshing state`() {
    val oldViewState = ShowNotesViewState(
        loadNoteStatus = LoadNoteStatus.REFRESH_FAIL,
        notes = emptyList(),
        loadNoteError = LoadNoteError.DATABASE_ERROR
    )
    viewModel.stateHolder.state = oldViewState

    viewModel.push(ShowNotesEvent.RefreshNotes)
    stateObserver.lastButOne shouldEqual ShowNotesViewState(
        loadNoteStatus = LoadNoteStatus.REFRESHING,
        notes = oldViewState.notes,
        loadNoteError = LoadNoteError.NO_ERROR
    )
  }

  @Test
  fun `on refresh notes, the view model must emit the refreshed notes`() {
    val oldViewState = ShowNotesViewState(
        LoadNoteStatus.SUCCESS,
        listOf(noteTemplate.copy(UUID.randomUUID()))
    )
    viewModel.stateHolder.state = oldViewState

    viewModel.push(ShowNotesEvent.RefreshNotes)

    val expected = listOf(
        repository.notes[2],
        repository.notes[1],
        repository.notes[0]
    )

    stateObserver.latest shouldEqual ShowNotesViewState(
        loadNoteStatus = LoadNoteStatus.SUCCESS,
        notes = expected,
        loadNoteError = LoadNoteError.NO_ERROR
    )
  }

  @Test
  fun `on pushing the add note action, the view model must emit a navigation action to open the create note screen`() {
    viewModel.push(ShowNotesEvent.AddNote)
    navigationObserver.hasChanges.shouldBeTrue()
    navigationObserver.latest shouldBe ShowNotesSideEffect.OpenCreateNoteScreen
  }

  @Test
  fun `on pushing the open note action, the view model must emit a navigation action to open the note detail screen`() {
    val noteToOpen = repository.notes[1]
    viewModel.push(ShowNotesEvent.OpenNote(noteToOpen))
    navigationObserver.hasChanges.shouldBeTrue()
    navigationObserver.latest shouldEqual ShowNotesSideEffect.ShowNoteScreen(noteToOpen.id)
  }
}
