package com.vinaysshenoy.spekex.threading

import kotlinx.coroutines.experimental.Runnable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class AsyncExecutor(
    private val delegate: Delegate = AsyncDelegate()
) {

  fun <T> async(
      job: () -> T,
      processResult: (T) -> Unit,
      processError: (Throwable) -> Unit = { throw it }
  ) {
    delegate.runOnBackground(Runnable {
      try {
        val result = job()
        delegate.runOnProcessor(Runnable { processResult(result) })
      } catch (e: Throwable) {
        delegate.runOnProcessor(Runnable { processError(e) })
      }
    })
  }

  fun process(job: () -> Unit) {
    delegate.runOnProcessor(Runnable { job() })
  }

  interface Delegate {

    fun runOnBackground(runnable: Runnable)

    fun runOnProcessor(runnable: Runnable)
  }

  class AsyncDelegate(
      private val background: ExecutorService = Executors.newCachedThreadPool(),
      private val process: ExecutorService = Executors.newSingleThreadExecutor()
  ) : Delegate {

    override fun runOnBackground(runnable: Runnable) {
      background.submit(runnable)
    }

    override fun runOnProcessor(runnable: Runnable) {
      process.submit(runnable)
    }
  }

  // Useful for testing
  class SynchronousDelegate : Delegate {

    override fun runOnBackground(runnable: Runnable) {
      runnable.run()
    }

    override fun runOnProcessor(runnable: Runnable) {
      runnable.run()
    }
  }
}