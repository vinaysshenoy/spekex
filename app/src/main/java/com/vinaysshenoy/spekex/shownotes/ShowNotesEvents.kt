package com.vinaysshenoy.spekex.shownotes

import com.vinaysshenoy.spekex.data.Note
import java.util.UUID

sealed class ShowNotesEvent {
  object RefreshNotes : ShowNotesEvent()
  object AddNote: ShowNotesEvent()
  data class OpenNote(val note: Note): ShowNotesEvent()
}

sealed class ShowNotesSideEffect {
  data class ShowNoteScreen(val id: UUID): ShowNotesSideEffect()
  object OpenCreateNoteScreen: ShowNotesSideEffect()
}
