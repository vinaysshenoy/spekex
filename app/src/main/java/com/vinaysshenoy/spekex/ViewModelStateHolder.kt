package com.vinaysshenoy.spekex

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

class ViewModelStateHolder<T>(startWith: T, testObserver: Observer<T>? = null) {

  private val liveDataInternal = MutableLiveData<T>()

  val liveData = liveDataInternal as LiveData<T>

  @get:VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
  @set:VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
  var state = startWith

  init {
    if (testObserver != null) {
      liveDataInternal.observeForever(testObserver)
    }
    liveDataInternal.postValue(state)
  }

  fun update(func: (T) -> T) {
    state = func(state)
    liveDataInternal.postValue(state)
  }
}