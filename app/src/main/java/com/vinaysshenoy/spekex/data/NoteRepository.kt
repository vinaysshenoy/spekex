package com.vinaysshenoy.spekex.data

import java.util.Date
import java.util.UUID

data class NoteNotFoundException(val uuid: UUID): RuntimeException("Could not find note for ID: $uuid")

interface NoteRepository {

  fun allNotes(newestFirst: Boolean): List<Note>

  fun getNote(id: UUID): Note

  fun createNote(note: Note)

  fun updateNote(note: Note, dateCreator: () -> Date = { Date() })

  fun deleteNotes(ids: List<UUID>)
}
