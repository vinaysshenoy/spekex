package com.vinaysshenoy.spekex.junit

import com.vinaysshenoy.spekex.data.Note
import com.vinaysshenoy.spekex.data.NoteNotFoundException
import com.vinaysshenoy.spekex.data.NoteRepositoryImpl
import com.vinaysshenoy.spekex.data.Timestamp
import org.amshove.kluent.shouldEqual
import org.amshove.kluent.shouldThrow
import org.junit.Test
import java.util.Date
import java.util.UUID

class NoteRepositoryTest {

  private lateinit var repository: NoteRepositoryImpl

  private val noteTemplate = Note(
      id = UUID.randomUUID(),
      title = "",
      content = "",
      timestamp = Timestamp()
  )

  @Test
  fun `when getting the list of notes, it should return them as per the order specified`() {
    val now = Date()

    val notes = listOf(
        noteTemplate.copy(UUID.randomUUID(), title = "Note 1", timestamp = Timestamp(updated = Date(now.time))),
        noteTemplate.copy(UUID.randomUUID(), title = "Note 2", timestamp = Timestamp(updated = Date(now.time + 10L))),
        noteTemplate.copy(UUID.randomUUID(), title = "Note 3", timestamp = Timestamp(updated = Date(now.time + 5L)))
    )

    repository = NoteRepositoryImpl(notes)

    repository.allNotes(newestFirst = true) shouldEqual listOf(notes[1], notes[2], notes[0])
    repository.allNotes(newestFirst = false) shouldEqual listOf(notes[0], notes[2], notes[1])
  }

  @Test
  fun `when getting a note by id for a note that exists, it should return the note`() {
    val note1 = noteTemplate.copy(UUID.randomUUID())
    val note2 = noteTemplate.copy(UUID.randomUUID())

    repository = NoteRepositoryImpl(listOf(note1, note2))

    repository.getNote(note2.id) shouldEqual note2.copy()
  }

  @Test
  fun `when getting a note that does not exist, it should throw a NoteNotFoundException`() {
    repository = NoteRepositoryImpl(emptyList())

    val uuid = UUID.randomUUID()
    val func = { repository.getNote(uuid) }
    func shouldThrow NoteNotFoundException(uuid)
  }

  @Test
  fun `when creating a note, it should add the note to the list of notes`() {
    val now = Date()

    val notes = listOf(
        noteTemplate.copy(UUID.randomUUID(), timestamp = Timestamp(updated = Date(now.time - 10))),
        noteTemplate.copy(UUID.randomUUID(), timestamp = Timestamp(updated = Date(now.time - 20)))
    )
    repository = NoteRepositoryImpl(notes)

    val noteToCreate = noteTemplate.copy(UUID.randomUUID(), timestamp = Timestamp(updated = Date(now.time)))
    repository.createNote(noteToCreate)

    repository.allNotes(true) shouldEqual listOf(noteToCreate, notes[0], notes[1])
  }

  @Test
  fun `when updating a note, it should save the updated note`() {
    val now = Date()

    val notes = listOf(
        noteTemplate.copy(UUID.randomUUID(), timestamp = Timestamp(updated = Date(now.time - 10))),
        noteTemplate.copy(UUID.randomUUID(), timestamp = Timestamp(updated = Date(now.time - 20)))
    )
    repository = NoteRepositoryImpl(notes)

    val noteToUpdate = notes[1].copy(title = "Title 2")
    repository.updateNote(noteToUpdate) { Date(now.time) }
    val expectedNote = noteToUpdate.copy(timestamp = noteToUpdate.timestamp.copy(updated = Date(now.time)))

    repository.allNotes(true) shouldEqual listOf(expectedNote, notes[0])
  }

  @Test
  fun `when updating a note that doesn't exist, it should throw a NoteNotFoundException`() {
    repository = NoteRepositoryImpl(emptyList())

    val uuid = UUID.randomUUID()
    val func = { repository.updateNote(noteTemplate.copy(id = uuid)) }
    func shouldThrow NoteNotFoundException(uuid)
  }

  @Test
  fun `when deleting a list of notes, it should delete only those notes`() {
    val now = Date()

    val notes = listOf(
        noteTemplate.copy(UUID.randomUUID(), title = "Note 1", timestamp = Timestamp(updated = Date(now.time))),
        noteTemplate.copy(UUID.randomUUID(), title = "Note 2", timestamp = Timestamp(updated = Date(now.time - 1))),
        noteTemplate.copy(UUID.randomUUID(), title = "Note 3", timestamp = Timestamp(updated = Date(now.time - 2))),
        noteTemplate.copy(UUID.randomUUID(), title = "Note 4", timestamp = Timestamp(updated = Date(now.time - 4)))
    )

    repository = NoteRepositoryImpl(notes)

    repository.deleteNotes(listOf(notes[1].id, notes[3].id))
    repository.allNotes(true) shouldEqual listOf(notes[0], notes[2])
  }
}
