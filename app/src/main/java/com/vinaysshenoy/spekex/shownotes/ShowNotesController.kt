package com.vinaysshenoy.spekex.shownotes

import androidx.annotation.VisibleForTesting
import com.vinaysshenoy.spekex.Controller
import com.vinaysshenoy.spekex.Receiver
import com.vinaysshenoy.spekex.data.Note
import com.vinaysshenoy.spekex.data.NoteRepository
import kotlinx.coroutines.experimental.DefaultDispatcher
import kotlinx.coroutines.experimental.IO
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.channels.actor
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import timber.log.Timber
import kotlin.coroutines.experimental.CoroutineContext

class ShowNotesController(
  private val noteRepository: NoteRepository,
  private val background: CoroutineContext = DefaultDispatcher,
  private val io: CoroutineContext = IO,
  ui: CoroutineContext = UI
) : Controller<ShowNotesEvent, ShowNotesViewState, ShowNotesSideEffect> {

  @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
  val reducer = actor<(ShowNotesViewState) -> ShowNotesViewState>(background) {
    var state = ShowNotesViewState(loadNoteStatus = LoadNoteStatus.LOADING, notes = emptyList())
    for (reducer in channel) {
      state = reducer(state)
      Timber.tag("BUG")
          .d("Reduced State: ${state.loadNoteStatus}")
      stateEmitter.offer(state)
    }
  }

  private val sideEffectEmitter = actor<ShowNotesSideEffect>(ui) {
    for (sideEffect in channel) {
      sideEffectReceiver?.receive(sideEffect)
    }
  }

  private val stateEmitter = actor<ShowNotesViewState>(ui) {
    for (state in channel) {
      Timber.tag("BUG")
          .d("Emitting State: ${state.loadNoteStatus} to emitter: $stateReceiver")
      stateReceiver?.receive(state)
    }
  }

  private var stateReceiver: Receiver<ShowNotesViewState>? = null
  private var sideEffectReceiver: Receiver<ShowNotesSideEffect>? = null

  private suspend fun loadNotes() {
    try {
      val notes = withContext(io) { noteRepository.allNotes(true) }
      reducer.offer { it.copy(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = notes) }
    } catch (e: Exception) {
      reducer.offer { it.copy(loadNoteStatus = LoadNoteStatus.LOAD_FAIL, loadNoteError = LoadNoteError.DATABASE_ERROR) }
    }
  }

  private suspend fun refreshNotes() {
    reducer.offer { it.copy(loadNoteStatus = LoadNoteStatus.REFRESHING, loadNoteError = LoadNoteError.NO_ERROR) }

    try {
      val notes = withContext(io) { noteRepository.allNotes(true) }
      reducer.offer { it.copy(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = notes) }
    } catch (e: Exception) {
      reducer.offer {
        it.copy(
            loadNoteStatus = LoadNoteStatus.REFRESH_FAIL, loadNoteError = LoadNoteError.DATABASE_ERROR
        )
      }
    }
  }

  private fun addNewNote() {
    sideEffectEmitter.offer(ShowNotesSideEffect.OpenCreateNoteScreen)
  }

  private fun openNote(note: Note) {
    sideEffectEmitter.offer(ShowNotesSideEffect.ShowNoteScreen(note.id))
  }

  override fun push(userEvent: ShowNotesEvent) {
    launch(background) {
      when (userEvent) {
        ShowNotesEvent.RefreshNotes -> refreshNotes()
        ShowNotesEvent.AddNote -> addNewNote()
        is ShowNotesEvent.OpenNote -> openNote(userEvent.note)
      }
    }
  }

  override fun registerStateReceiver(
    receiver: Receiver<ShowNotesViewState>,
    initial: Boolean
  ) {
    stateReceiver = receiver
    if (initial) {
      Timber.tag("BUG")
          .d("Loading notes")
      launch(background) {
        reducer.offer {
          Timber.tag("BUG")
              .d("Reduce initial state: $it")
          it
        }
        loadNotes()
      }
    }
  }

  override fun unregisterStateReceiver() {
    stateReceiver = null
  }

  override fun registerSideEffectReceiver(receiver: Receiver<ShowNotesSideEffect>) {
    sideEffectReceiver = receiver
  }

  override fun unregisterSideEffectReceiver() {
    sideEffectReceiver = null
  }

  override fun destroy() {
    reducer.close()
    sideEffectEmitter.close()
    stateEmitter.close()
  }
}
