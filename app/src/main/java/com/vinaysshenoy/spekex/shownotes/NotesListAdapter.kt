package com.vinaysshenoy.spekex.shownotes

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.vinaysshenoy.spekex.data.Note

class NotesListAdapter(private val noteClicked: (Note) -> Unit) : ListAdapter<Note, ShowNoteListItemViewHolder>(NoteDiffCallback()) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ShowNoteListItemViewHolder(parent, noteClicked)

  override fun onBindViewHolder(holder: ShowNoteListItemViewHolder, position: Int) {
    holder.render(getItem(position))
  }
}

private class NoteDiffCallback : DiffUtil.ItemCallback<Note>() {

  override fun areItemsTheSame(oldItem: Note, newItem: Note) = oldItem.id == newItem.id

  override fun areContentsTheSame(oldItem: Note, newItem: Note) = oldItem == newItem
}