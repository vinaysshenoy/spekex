package com.vinaysshenoy.spekex.shownotes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.vinaysshenoy.spekex.R
import com.vinaysshenoy.spekex.data.Note
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.listitem_note.*

class ShowNoteListItemViewHolder private constructor(
    override val containerView: View,
    private val noteClicked: (Note) -> Unit
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

  constructor(parent: ViewGroup, noteClicked: (Note) -> Unit): this(
      containerView = LayoutInflater.from(parent.context).inflate(R.layout.listitem_note, parent, false),
      noteClicked = noteClicked
  )

  private var note: Note? = null

  init {
    itemView.setOnClickListener { _ -> note.takeIf { it != null }?.let { noteClicked(it) } }
    setCaptionDrawableOnTimeText()
  }

  private fun setCaptionDrawableOnTimeText() {
    val captionDrawable = ResourcesCompat.getDrawable(
        itemView.resources,
        R.drawable.ic_access_time_white_12dp,
        itemView.context.theme
    )!!

    val captionColor = ResourcesCompat.getColor(
        itemView.resources,
        R.color.colorCaption,
        itemView.context.theme
    )
    DrawableCompat.setTint(captionDrawable, captionColor)
    tv_noteTime.setCompoundDrawablesWithIntrinsicBounds(captionDrawable, null, null, null)
    tv_noteTime.compoundDrawablePadding = itemView.resources.getDimensionPixelSize(R.dimen.captionDrawablePadding)
  }

  fun render(note: Note) {
    this.note = note
    tv_noteTitle.text = note.title
    tv_noteContent.text = note.content
    // FIXME: Format the date properly
    tv_noteTime.text = note.timestamp.updated.toString()
  }
}