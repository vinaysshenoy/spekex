package com.vinaysshenoy.spekex

import java.util.UUID

interface Navigator {

  fun showNotesList()

  fun showCreateNote()

  fun showNoteDetail(noteId: UUID)
}