package com.vinaysshenoy.spekex.shownotes

import com.vinaysshenoy.spekex.data.Note

data class ShowNotesViewState(
  val loadNoteStatus: LoadNoteStatus,
  val notes: List<Note>,
  val loadNoteError: LoadNoteError = LoadNoteError.NO_ERROR
)

enum class LoadNoteError {
  NO_ERROR,
  DATABASE_ERROR
}

enum class LoadNoteStatus {
  LOADING,
  LOAD_FAIL,
  SUCCESS,
  REFRESHING,
  REFRESH_FAIL
}
