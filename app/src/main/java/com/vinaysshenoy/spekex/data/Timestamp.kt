package com.vinaysshenoy.spekex.data

import java.util.Date

data class Timestamp(
    val created: Date = Date(),
    val updated: Date = Date()
) {

  fun withUpdatedOn(date: Date) = copy(updated = date)
}
