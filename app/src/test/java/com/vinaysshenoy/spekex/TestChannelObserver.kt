package com.vinaysshenoy.spekex

class TestChannelObserver<T> {

  var events = emptyList<T>()
    private set

  val latest: T
    get() = events.last()

  val lastButOne: T
    get() = events[events.lastIndex - 1]

  val hasChanges: Boolean
    get() = events.isNotEmpty()

  fun onChanged(change: T) {
    events += change
  }

  fun reset() {
    events = emptyList()
  }
}