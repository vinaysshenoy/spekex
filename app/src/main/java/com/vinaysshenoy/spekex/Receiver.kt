package com.vinaysshenoy.spekex

interface Receiver<T: Any> {

  fun receive(item: T)
}