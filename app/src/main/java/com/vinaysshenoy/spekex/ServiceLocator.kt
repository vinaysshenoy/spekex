package com.vinaysshenoy.spekex

import androidx.lifecycle.ViewModelProvider
import com.vinaysshenoy.spekex.data.NoteRepository
import com.vinaysshenoy.spekex.threading.AsyncExecutor

interface ServiceLocator {

  val notesRepository: NoteRepository

  val asyncExecutor: AsyncExecutor

  val viewModelFactory: ViewModelProvider.Factory
}