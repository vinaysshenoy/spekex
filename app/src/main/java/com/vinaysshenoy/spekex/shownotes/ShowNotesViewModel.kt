package com.vinaysshenoy.spekex.shownotes

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.vinaysshenoy.spekex.ViewModelStateHolder
import com.vinaysshenoy.spekex.data.Note
import com.vinaysshenoy.spekex.data.NoteRepository
import com.vinaysshenoy.spekex.threading.AsyncExecutor

class ShowNotesViewModel(
    private val notesRepository: NoteRepository,
    private val async: AsyncExecutor,
    testObserver: Observer<ShowNotesViewState>? = null
) : ViewModel() {

  @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
  val stateHolder = ViewModelStateHolder(
      startWith = ShowNotesViewState(loadNoteStatus = LoadNoteStatus.LOADING, notes = emptyList()),
      testObserver = testObserver
  )

  @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
  val navigationActions = MutableLiveData<ShowNotesSideEffect>()

  init {
    loadNotes()
  }

  fun pull() = stateHolder.liveData

  fun push(event: ShowNotesEvent) {
    when (event) {
      ShowNotesEvent.RefreshNotes -> refreshNotes()
      ShowNotesEvent.AddNote -> addNewNote()
      is ShowNotesEvent.OpenNote -> openNote(event.note)
    }
  }

  fun navigationEvents() = navigationActions as LiveData<ShowNotesSideEffect>

  private fun addNewNote() {
    async.process { navigationActions.postValue(ShowNotesSideEffect.OpenCreateNoteScreen) }
  }

  private fun openNote(note: Note) {
    async.process { navigationActions.postValue(ShowNotesSideEffect.ShowNoteScreen(note.id)) }
  }

  private fun refreshNotes() {
    async.process { stateHolder.update { it.copy(loadNoteStatus = LoadNoteStatus.REFRESHING, loadNoteError = LoadNoteError.NO_ERROR) } }
    async.async(
        job = { notesRepository.allNotes(true) },
        processResult = { notes ->
          stateHolder.update { it.copy(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = notes) }
        },
        processError = { _ ->
          stateHolder.update { it.copy(loadNoteStatus = LoadNoteStatus.REFRESH_FAIL, loadNoteError = LoadNoteError.DATABASE_ERROR) }
        }
    )
  }

  private fun loadNotes() {
    async.async(
        job = { notesRepository.allNotes(true) },
        processResult = { notes ->
          stateHolder.update { it.copy(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = notes) }
        },
        processError = { _ ->
          stateHolder.update { it.copy(loadNoteStatus = LoadNoteStatus.LOAD_FAIL, loadNoteError = LoadNoteError.DATABASE_ERROR) }
        }
    )
  }
}