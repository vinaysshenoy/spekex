package com.vinaysshenoy.spekex.gameoflife

data class GameOfLifeState(val cells: List<Cell>)
