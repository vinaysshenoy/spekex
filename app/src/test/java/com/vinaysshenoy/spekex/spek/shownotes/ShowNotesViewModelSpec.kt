package com.vinaysshenoy.spekex.spek.shownotes

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.testing.InstantTaskExecutor
import com.vinaysshenoy.spekex.TestObserver
import com.vinaysshenoy.spekex.data.Note
import com.vinaysshenoy.spekex.data.Timestamp
import com.vinaysshenoy.spekex.mocks.MockNoteRepository
import com.vinaysshenoy.spekex.shownotes.LoadNoteError
import com.vinaysshenoy.spekex.shownotes.LoadNoteStatus
import com.vinaysshenoy.spekex.shownotes.ShowNotesEvent
import com.vinaysshenoy.spekex.shownotes.ShowNotesSideEffect
import com.vinaysshenoy.spekex.shownotes.ShowNotesViewModel
import com.vinaysshenoy.spekex.shownotes.ShowNotesViewState
import com.vinaysshenoy.spekex.threading.AsyncExecutor
import org.amshove.kluent.shouldBeFalse
import org.amshove.kluent.shouldBeTrue
import org.amshove.kluent.shouldEqual
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.jetbrains.spek.api.lifecycle.CachingMode
import java.util.Date
import java.util.UUID

object ShowNotesViewModelSpec : Spek({

  describe("setup") {

    beforeGroup {
      ArchTaskExecutor.getInstance().setDelegate(InstantTaskExecutor())
    }

    val noteTemplate = Note(
        id = UUID.randomUUID(),
        title = "",
        content = "",
        timestamp = Timestamp()
    )

    val async by memoized(CachingMode.SCOPE) { AsyncExecutor(AsyncExecutor.SynchronousDelegate()) }
    val stateObserver by memoized { TestObserver<ShowNotesViewState>() }
    val navigationObserver by memoized { TestObserver<ShowNotesSideEffect>() }

    describe("a NoteRepository that works") {
      val date = Date()
      val notes = listOf(
          noteTemplate.copy(UUID.randomUUID(), timestamp = noteTemplate.timestamp.withUpdatedOn(Date(date.time))),
          noteTemplate.copy(UUID.randomUUID(), timestamp = noteTemplate.timestamp.withUpdatedOn(Date(date.time + 1))),
          noteTemplate.copy(UUID.randomUUID(), timestamp = noteTemplate.timestamp.withUpdatedOn(Date(date.time + 2)))
      )

      val repository by memoized(CachingMode.SCOPE) {
        object : MockNoteRepository() {
          override fun allNotes(newestFirst: Boolean) =
              if (newestFirst) listOf(notes[2], notes[1], notes[0]) else listOf(notes[0], notes[1], notes[2])
        }
      }
      val viewModel by memoized { ShowNotesViewModel(repository, async, stateObserver) }

      beforeEachTest {
        viewModel.navigationEvents().observeForever(navigationObserver)
      }

      on("initializing the view model") {

        it("should have changes") {
          stateObserver.hasChanges.shouldBeTrue()
        }

        it("should emit the initial state") {
          stateObserver.changes.first() shouldEqual ShowNotesViewState(
              loadNoteStatus = LoadNoteStatus.LOADING,
              notes = emptyList(),
              loadNoteError = LoadNoteError.NO_ERROR
          )
        }

        it("must not emit any navigation actions") {
          navigationObserver.hasChanges.shouldBeFalse()
        }

        it("must load notes in the order of latest updated") {
          val expected = listOf(notes[2], notes[1], notes[0])

          stateObserver.latest shouldEqual ShowNotesViewState(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = expected, loadNoteError = LoadNoteError.NO_ERROR)
        }
      }

      group("refreshing notes with previous error") {
        val testCases = mapOf(
            LoadNoteStatus.LOAD_FAIL to LoadNoteError.NO_ERROR,
            LoadNoteStatus.REFRESH_FAIL to LoadNoteError.NO_ERROR
        )

        testCases.forEach { loadNoteStatus, expectedLoadNoteError ->
          on("previous error is $loadNoteStatus") {
            val oldViewState = ShowNotesViewState(
                loadNoteStatus = loadNoteStatus,
                notes = emptyList(),
                loadNoteError = LoadNoteError.DATABASE_ERROR
            )

            viewModel.stateHolder.state = oldViewState

            viewModel.push(ShowNotesEvent.RefreshNotes)

            it("should reset the load note error to $expectedLoadNoteError when emitting the refreshing state") {
              stateObserver.lastButOne shouldEqual ShowNotesViewState(
                  loadNoteStatus = LoadNoteStatus.REFRESHING,
                  notes = emptyList(),
                  loadNoteError = expectedLoadNoteError
              )
            }

            it("should emit the refreshed view state with the notes ordered by latest updated") {
              val expected = listOf(notes[2], notes[1], notes[0])
              stateObserver.latest shouldEqual ShowNotesViewState(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = expected, loadNoteError = LoadNoteError.NO_ERROR)
            }
          }
        }
      }

      on("refreshing notes without previous error") {
        val oldViewState = ShowNotesViewState(
            loadNoteStatus = LoadNoteStatus.SUCCESS,
            notes = listOf(noteTemplate.copy(UUID.randomUUID())),
            loadNoteError = LoadNoteError.NO_ERROR
        )

        viewModel.stateHolder.state = oldViewState

        viewModel.push(ShowNotesEvent.RefreshNotes)

        it("should emit the refreshing view state") {
          stateObserver.lastButOne shouldEqual ShowNotesViewState(loadNoteStatus = LoadNoteStatus.REFRESHING, notes = oldViewState.notes, loadNoteError = LoadNoteError.NO_ERROR)
        }

        it("should emit the refreshed state with the notes ordered by latest updated") {
          val expected = listOf(notes[2], notes[1], notes[0])
          stateObserver.latest shouldEqual ShowNotesViewState(loadNoteStatus = LoadNoteStatus.SUCCESS, notes = expected, loadNoteError = LoadNoteError.NO_ERROR)
        }
      }

      on("pushing the add note action") {
        viewModel.push(ShowNotesEvent.AddNote)

        it("should emit the open note create navigation event") {
          navigationObserver.latest shouldEqual ShowNotesSideEffect.OpenCreateNoteScreen
        }
      }

      on("pushing the open note action") {
        val noteToOpen = notes[0]
        viewModel.push(ShowNotesEvent.OpenNote(noteToOpen))

        it("should emit the show note screen with the note id") {
          navigationObserver.latest shouldEqual ShowNotesSideEffect.ShowNoteScreen(noteToOpen.id)
        }
      }
    }

    describe("a repository that fails") {
      val repository by memoized(CachingMode.SCOPE) {
        object : MockNoteRepository() {
          override fun allNotes(newestFirst: Boolean): List<Note> {
            throw RuntimeException("Test Exception")
          }
        }
      }

      val viewModel by memoized { ShowNotesViewModel(repository, async, stateObserver) }

      beforeEachTest {
        viewModel.navigationEvents().observeForever(navigationObserver)
      }

      on("initializing the view model") {
        it("must emit the failed state") {
          stateObserver.latest shouldEqual ShowNotesViewState(
              loadNoteStatus = LoadNoteStatus.LOAD_FAIL,
              notes = emptyList(),
              loadNoteError = LoadNoteError.DATABASE_ERROR
          )
        }
      }

      on("refreshing notes with previous notes present") {
        val oldViewState = ShowNotesViewState(
            loadNoteStatus = LoadNoteStatus.SUCCESS,
            notes = listOf(noteTemplate.copy(UUID.randomUUID())),
            loadNoteError = LoadNoteError.NO_ERROR
        )

        viewModel.stateHolder.state = oldViewState
        viewModel.push(ShowNotesEvent.RefreshNotes)

        it("should emit the failed state with the previous notes") {
          stateObserver.latest shouldEqual ShowNotesViewState(
              loadNoteStatus = LoadNoteStatus.REFRESH_FAIL,
              notes = oldViewState.notes,
              loadNoteError = LoadNoteError.DATABASE_ERROR
          )
        }
      }
    }
  }

})