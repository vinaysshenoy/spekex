package com.vinaysshenoy.spekex.mocks

import com.vinaysshenoy.spekex.data.Note
import com.vinaysshenoy.spekex.data.NoteRepository
import java.util.Date
import java.util.UUID

open class MockNoteRepository: NoteRepository {

  override fun allNotes(newestFirst: Boolean) = emptyList<Note>()

  override fun getNote(id: UUID): Note {
    throw NotImplementedError("Not Implemented")
  }

  override fun createNote(note: Note) {}

  override fun updateNote(note: Note, dateCreator: () -> Date) {}

  override fun deleteNotes(ids: List<UUID>) {}
}