package com.vinaysshenoy.spekex

interface MviView<T: Any, P: Any> {

  fun render(state: T)

  fun sideEffect(sideEffect: P)
}