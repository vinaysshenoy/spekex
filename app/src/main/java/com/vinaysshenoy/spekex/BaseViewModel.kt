package com.vinaysshenoy.spekex

import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.channels.actor
import kotlin.coroutines.experimental.CoroutineContext

abstract class BaseViewModel<T : Any>(initialState: T, coroutineContext: CoroutineContext = CommonPool) : ViewModel() {

  private val reduceActor = actor<(T) -> T>(coroutineContext) {
    var state: T = initialState
    for (reducer in channel) {
      state = reducer(state)
      stateEmitter.postValue(state)
    }
  }

  private val stateEmitter = MutableLiveData<T>()

  init {
    stateEmitter.postValue(initialState)
  }

  @CallSuper
  override fun onCleared() {
    super.onCleared()
    reduceActor.close()
  }

  protected suspend fun update(reducer: (T) -> T) {
    reduceActor.send(reducer)
  }

  fun pull(): LiveData<T> = stateEmitter
}
