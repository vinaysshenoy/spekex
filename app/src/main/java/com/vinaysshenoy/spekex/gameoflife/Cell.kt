package com.vinaysshenoy.spekex.gameoflife

data class Cell(val x: Int, val y: Int, val alive: Boolean)
