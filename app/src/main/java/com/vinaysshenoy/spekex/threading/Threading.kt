package com.vinaysshenoy.spekex.threading

import kotlinx.coroutines.experimental.DefaultDispatcher
import kotlinx.coroutines.experimental.IO
import kotlinx.coroutines.experimental.async
import kotlin.coroutines.experimental.CoroutineContext

private var backgroundContext: CoroutineContext = DefaultDispatcher
private var ioContext: CoroutineContext = IO

private fun <T> background(block: () -> T) = async(backgroundContext) { block() }

private fun <T> io(block: () -> T) = async(ioContext) { block() }
