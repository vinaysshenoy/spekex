package com.vinaysshenoy.spekex.data

import java.util.Date
import java.util.Random
import java.util.UUID

class NoteRepositoryImpl(var notes: List<Note> = emptyList()) : NoteRepository {

  private val random = Random()

  private fun sleepForRandomSeconds(min: Float = 10f, max: Float = 15f) {
    val timeToSleep = min + random.nextFloat() * (max - min)
    Thread.sleep((timeToSleep * 1000).toLong())
  }

  override fun allNotes(newestFirst: Boolean): List<Note> {
    sleepForRandomSeconds()
    return if (newestFirst) {
      notes.sortedByDescending { it.timestamp.updated }
    } else {
      notes.sortedBy { it.timestamp.updated }
    }
  }

  override fun getNote(id: UUID): Note {
    sleepForRandomSeconds()
    return notes.find { it.id == id } ?: throw NoteNotFoundException(id)
  }

  override fun createNote(note: Note) {
    sleepForRandomSeconds()
    notes += note
  }

  override fun updateNote(note: Note, dateCreator: () -> Date) {
    sleepForRandomSeconds()
    val localNote = notes.find { it.id == note.id } ?: throw NoteNotFoundException(note.id)

    notes -= localNote
    notes += note.copy(timestamp = localNote.timestamp.copy(updated = dateCreator()))
  }

  override fun deleteNotes(ids: List<UUID>) {
    sleepForRandomSeconds()
    notes = notes.filter { it.id !in ids }
  }
}
