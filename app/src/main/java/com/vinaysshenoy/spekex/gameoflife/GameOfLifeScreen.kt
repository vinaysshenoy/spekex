package com.vinaysshenoy.spekex.gameoflife

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.vinaysshenoy.spekex.Controller
import com.vinaysshenoy.spekex.MviView
import com.vinaysshenoy.spekex.R
import com.vinaysshenoy.spekex.ViewControllerBinding
import kotlinx.android.synthetic.main.screen_gameoflife.widget

class GameOfLifeScreen : Fragment(), MviView<GameOfLifeState, GameOfLifeSideEffect> {

  lateinit var controller: Controller<GameOfLifeUserEvent, GameOfLifeState, GameOfLifeSideEffect>

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    controller = GameOfLifeControllerImpl(
        size = 16,
        seed = setOf(
            8 to 8,
            9 to 8,
            9 to 7,
            9 to 6,
            7 to 7
        )
    )
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    viewLifecycleOwner.lifecycle.addObserver(ViewControllerBinding(controller, this))
    return inflater.inflate(R.layout.screen_gameoflife, container, false)
  }

  override fun render(state: GameOfLifeState) {
    widget.cells = state.cells
  }

  override fun sideEffect(sideEffect: GameOfLifeSideEffect) {

  }
}
