package com.vinaysshenoy.spekex

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vinaysshenoy.spekex.data.Note
import com.vinaysshenoy.spekex.data.NoteRepositoryImpl
import com.vinaysshenoy.spekex.shownotes.ShowNotesController
import com.vinaysshenoy.spekex.shownotes.ShowNotesViewModel
import com.vinaysshenoy.spekex.threading.AsyncExecutor
import timber.log.Timber
import java.util.UUID

class TheApplication : Application(), ServiceLocator {

  companion object {
    @JvmStatic
    lateinit var serviceLocator: ServiceLocator
  }

  override val notesRepository = NoteRepositoryImpl(
      notes = listOf(
          Note(id = UUID.randomUUID(), title = "Title 1", content = "This is some dummy text that nobody gives a damn about"),
          Note(id = UUID.randomUUID(), title = "Title 2", content = "This is some dummy text that nobody gives a damn about"),
          Note(id = UUID.randomUUID(), title = "Title 3", content = "This is some dummy text that nobody gives a damn about"),
          Note(id = UUID.randomUUID(), title = "Title 4", content = "This is some dummy text that nobody gives a damn about")
      )
  )

  override val asyncExecutor = AsyncExecutor()

  override val viewModelFactory = object : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
      return when {
        modelClass.isAssignableFrom(ShowNotesViewModel::class.java) -> ShowNotesViewModel(notesRepository, asyncExecutor)
        modelClass.isAssignableFrom(ShowNotesController::class.java) -> ShowNotesController(notesRepository)
        else -> null
      } as T
    }
  }

  override fun onCreate() {
    super.onCreate()
    serviceLocator = this
    if (BuildConfig.DEBUG) {
      Timber.plant(Timber.DebugTree())
    }
  }
}
