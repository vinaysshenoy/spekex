package com.vinaysshenoy.spekex

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vinaysshenoy.spekex.gameoflife.GameOfLifeScreen
import com.vinaysshenoy.spekex.shownotes.ShowNotesScreen
import kotlinx.android.synthetic.main.the_activity.*
import java.util.UUID

class TheActivity : AppCompatActivity() {

  val navigator = object : Navigator {
    override fun showNotesList() {
      supportFragmentManager.beginTransaction()
          .replace(R.id.frame_screens, GameOfLifeScreen(), "game_of_life")
          .commit()
    }

    override fun showCreateNote() {
    }

    override fun showNoteDetail(noteId: UUID) {
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.the_activity)
    setSupportActionBar(toolbar_app)

    if (savedInstanceState == null) {
      navigator.showNotesList()
    }
  }
}
